object svmEmployee: TsvmEmployee
  OldCreateOrder = False
  Height = 197
  Width = 391
  object conFB: TFDConnection
    Params.Strings = (
      'Database=Employee'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    FetchOptions.AssignedValues = [evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckForwardOnly
    FetchOptions.Unidirectional = True
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 56
    Top = 32
  end
  object qryEmployee: TFDQuery
    Connection = conFB
    SQL.Strings = (
      'Select Emp_No, First_Name, Last_Name, Salary'
      'From Employee'
      'Where First_Name Like :Name'
      'Order By First_Name')
    Left = 144
    Top = 32
    ParamData = <
      item
        Name = 'NAME'
        DataType = ftString
        ParamType = ptInput
        Size = 15
        Value = Null
      end>
    object qryEmployeeEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryEmployeeFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object qryEmployeeLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object qryEmployeeSALARY: TBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
  end
  object dspEmployee: TDataSetProvider
    DataSet = qryEmployee
    Options = [poPropogateChanges]
    UpdateMode = upWhereKeyOnly
    BeforeUpdateRecord = dspEmployeeBeforeUpdateRecord
    Left = 240
    Top = 32
  end
end
