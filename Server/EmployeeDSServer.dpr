program EmployeeDSServer;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FMain in 'FMain.pas' {frmMain},
  SEmployeeMethods in 'SEmployeeMethods.pas' {svmEmployee: TDSServerModule},
  UServerContainer in 'UServerContainer.pas' {svrContainer: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TsvrContainer, svrContainer);
  Application.Run;
end.

