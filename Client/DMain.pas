unit DMain;

interface

uses
  System.SysUtils, System.Classes, Data.DBXDataSnap, IPPeerClient,
  Data.DBXCommon, Data.DB, Data.SqlExpr, Datasnap.DBClient, Datasnap.DSConnect;

type
  TdmoMain = class(TDataModule)
    conDSServer: TSQLConnection;
    cdsEmployee: TClientDataSet;
    dpcEmployee: TDSProviderConnection;
    cdsEmployeeEMP_NO: TSmallintField;
    cdsEmployeeFIRST_NAME: TStringField;
    cdsEmployeeLAST_NAME: TStringField;
    cdsEmployeeSALARY: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect(const IPAddress: string);
    procedure Disconnect;
    procedure Open(const Name: string);
    procedure GotoRecord(const RecNo: Integer);
    procedure Close;
    procedure ApplyUpates;
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TdmoMain }

procedure TdmoMain.ApplyUpates;
begin
  cdsEmployee.ApplyUpdates(0);
end;

procedure TdmoMain.Close;
begin
  cdsEmployee.Close;
end;

procedure TdmoMain.Connect(const IPAddress: string);
begin
  conDSServer.Params.Values['HostName'] := IPAddress;
  conDSServer.Open;
end;

procedure TdmoMain.Disconnect;
begin
  conDSServer.Close;
end;

procedure TdmoMain.GotoRecord(const RecNo: Integer);
begin
  cdsEmployee.RecNo := RecNo;
end;

procedure TdmoMain.Open(const Name: string);
begin
  cdsEmployee.ParamByName('Name').AsString := Name + '%';
  cdsEmployee.Open;
end;

end.
