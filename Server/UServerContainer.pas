unit UServerContainer;

interface

uses System.SysUtils, System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer, Datasnap.DSCommonServer,
  Datasnap.DSAuth, IPPeerServer;

type
  TsvrContainer = class(TDataModule)
    dsnServer: TDSServer;
    tcpTransport: TDSTCPServerTransport;
    sclEmployee: TDSServerClass;
    procedure sclEmployeeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  svrContainer: TsvrContainer;

implementation


{$R *.dfm}

uses
  SEmployeeMethods;

procedure TsvrContainer.sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                            var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmEmployee;
end;


end.

