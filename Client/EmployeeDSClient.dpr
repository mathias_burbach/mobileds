program EmployeeDSClient;

uses
  System.StartUpCopy,
  FMX.Forms,
  //MidasLib,
  FMain in 'FMain.pas' {frmMain},
  DMain in 'DMain.pas' {dmoMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmoMain, dmoMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
