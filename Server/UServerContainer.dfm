object svrContainer: TsvrContainer
  OldCreateOrder = False
  Height = 271
  Width = 415
  object dsnServer: TDSServer
    Left = 96
    Top = 11
  end
  object tcpTransport: TDSTCPServerTransport
    Server = dsnServer
    Filters = <>
    Left = 96
    Top = 73
  end
  object sclEmployee: TDSServerClass
    OnGetClass = sclEmployeeGetClass
    Server = dsnServer
    LifeCycle = 'Invocation'
    Left = 200
    Top = 11
  end
end
