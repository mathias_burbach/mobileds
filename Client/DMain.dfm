object dmoMain: TdmoMain
  OldCreateOrder = False
  Height = 259
  Width = 466
  object conDSServer: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=23.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b')
    Left = 64
    Top = 56
  end
  object cdsEmployee: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'NAME'
        ParamType = ptInput
        Size = 15
        Value = '%'
      end>
    ProviderName = 'dspEmployee'
    RemoteServer = dpcEmployee
    Left = 256
    Top = 56
    object cdsEmployeeEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsEmployeeFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object cdsEmployeeLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object cdsEmployeeSALARY: TBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
  end
  object dpcEmployee: TDSProviderConnection
    ServerClassName = 'TsvmEmployee'
    SQLConnection = conDSServer
    Left = 160
    Top = 56
  end
end
