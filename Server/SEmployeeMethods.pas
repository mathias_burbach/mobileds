unit SEmployeeMethods;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.Provider,
  Datasnap.DSServer,
  Datasnap.DBClient,
  DataSnap.DSProviderDataModuleAdapter,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TsvmEmployee = class(TDSServerModule)
    conFB: TFDConnection;
    qryEmployee: TFDQuery;
    qryEmployeeEMP_NO: TSmallintField;
    qryEmployeeFIRST_NAME: TStringField;
    qryEmployeeLAST_NAME: TStringField;
    qryEmployeeSALARY: TBCDField;
    dspEmployee: TDataSetProvider;
    procedure dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                            SourceDS: TDataSet;
                                            DeltaDS: TCustomClientDataSet;
                                            UpdateKind: TUpdateKind;
                                            var Applied: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
  end;

implementation


{$R *.dfm}


uses System.StrUtils;

procedure TsvmEmployee.dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                                     SourceDS: TDataSet;
                                                     DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind;
                                                     var Applied: Boolean);
var
  fld: TField;
begin
  if UpdateKind = ukModify then
  begin
    fld := DeltaDS.FieldByName('Salary');
    fld.NewValue := fld.OldValue + 1.00;
  end;
end;

function TsvmEmployee.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TsvmEmployee.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;

end.

