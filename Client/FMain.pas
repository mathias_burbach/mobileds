unit FMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Gestures, FMX.Controls.Presentation, FMX.Edit, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, FMX.Layouts,
  FMX.ListBox;

type
  TfrmMain = class(TForm)
    HeaderToolBar: TToolBar;
    ToolBarLabel: TLabel;
    tbcMain: TTabControl;
    tabConnect: TTabItem;
    tabList: TTabItem;
    tabDetails: TTabItem;
    btnConnect: TButton;
    edtIPAddress: TEdit;
    lblIPAddress: TLabel;
    btnDisconnect: TButton;
    Panel1: TPanel;
    edtFilterByName: TEdit;
    lblFilterByName: TLabel;
    btnSearchDB: TButton;
    lbxEmployee: TListBox;
    edtEmpNo: TEdit;
    edtFirstName: TEdit;
    edtLastName: TEdit;
    lblSalary: TLabel;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkSalary: TLinkPropertyToField;
    LinkLastName: TLinkControlToField;
    LinkFirstName: TLinkControlToField;
    LinkEmpNo: TLinkControlToField;
    btnApplyUpdates: TButton;
    LinkFillListBoxWithFirstName: TLinkFillControlToField;
    procedure FormCreate(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
    procedure btnSearchDBClick(Sender: TObject);
    procedure btnApplyUpdatesClick(Sender: TObject);
    procedure lbxEmployeeChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses
  DMain;

procedure TfrmMain.btnApplyUpdatesClick(Sender: TObject);
begin
  dmoMain.ApplyUpates;
  lbxEmployee.Selected.Text := edtFirstName.Text;
end;

procedure TfrmMain.btnConnectClick(Sender: TObject);
begin
  dmoMain.Connect(edtIPAddress.Text);
  btnConnect.Enabled := False;
  btnDisconnect.Enabled := True;
  btnSearchDB.Enabled := True;
end;

procedure TfrmMain.btnDisconnectClick(Sender: TObject);
begin
  dmoMain.Disconnect;
  btnConnect.Enabled := True;
  btnDisconnect.Enabled := False;
  btnSearchDB.Enabled := False;
end;

procedure TfrmMain.btnSearchDBClick(Sender: TObject);
begin
  dmoMain.Close;
  dmoMain.Open(edtFilterByName.Text);
  if lbxEmployee.Items.Count > 0 then
    lbxEmployee.ItemIndex := 0;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  tbcMain.ActiveTab := tabConnect;
end;

procedure TfrmMain.lbxEmployeeChange(Sender: TObject);
begin
  dmoMain.GotoRecord(lbxEmployee.ItemIndex + 1);
end;

end.
